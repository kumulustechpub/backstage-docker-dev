echo "POSTGRES_HOST=postgres
POSTGRES_PORT=5432
POSTGRES_DB=backstage
POSTGRES_USER=postgres" > .env
echo POSTGRES_PASSWORD=$(op read op://private/PostgreSQL-PW/password) >> .env
echo AUTH_GITLAB_CLIENT_ID=$(op read op://private/GitLab-Backstage/applicationId) >> .env
echo AUTH_GITLAB_CLIENT_SECRET=$(op read op://private/GitLab-Backstage/credential) >> .env
