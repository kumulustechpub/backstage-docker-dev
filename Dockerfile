FROM node:16-alpine

WORKDIR /usr/src/app
RUN echo backstage | npx @backstage/create-app@latest --path /usr/src/app \
    && yarn install \
    && yarn tsc \
    && yarn add --cwd packages/backend pg
COPY . .
EXPOSE 3000
CMD ["yarn", "dev"]
