# Backstage develpment environment with Docker/docker-compose

This is a repository to help containerize and develop a backstage environment that can be iterated on via docker/docker-compose.  The intent is to enable a solution that simplifies building out a production ready instance with GitHub and GitLab as user authentication, and with the ability to manage repositories in one/the other of those environments.

